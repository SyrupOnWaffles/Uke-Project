#include <iostream>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <string>
using namespace std;
#define log(x) cout << x;
int main()
{

   //! Instantiation
   if (!al_init())
   {
      log("couldn't initialize allegro\n");
      return 1;
   }
   if (!al_install_keyboard())
   {
      log("couldn't initialize keyboard\n");
      return 1;
   }
   if (!al_init_image_addon())
   {
      printf("couldn't initialize image addon\n");
      return 1;
   }
   if (!al_init_primitives_addon())
   {
      printf("couldn't initialize primative addon\n");
      return 1;
   }
   if (!al_install_audio())
   {
      printf("couldn't initialize audio\n");
      return 1;
   }
   if (!al_init_acodec_addon())
   {
      printf("couldn't initialize audio codecs\n");
      return 1;
   }
   if (!al_reserve_samples(64))
   {
      printf("couldn't initialize reserve samples\n");
      return 1;
   }

   ALLEGRO_TIMER *timer = al_create_timer(1.0 / 303);
   if (!timer)
   {
      log("couldn't initialize timer\n");
      return 1;
   }

   ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();
   if (!queue)
   {
      log("couldn't initialize queue\n");
      return 1;
   }

   ALLEGRO_DISPLAY *disp = al_create_display(1280, 720);
   if (!disp)
   {
      log("couldn't initialize display\n");
      return 1;
   }

   ALLEGRO_FONT *font = al_create_builtin_font();
   if (!font)
   {
      log("couldn't initialize font\n");
      return 1;
   }

   ALLEGRO_SAMPLE *fretboardSamples[2][8] = {
      {al_load_sample("sounds/3/3C.wav"), al_load_sample("sounds/3/3C#.wav"), al_load_sample("sounds/3/3D.wav"), al_load_sample("sounds/3/3D#.wav"), al_load_sample("sounds/3/3E.wav"), al_load_sample("sounds/3/3F.wav"), al_load_sample("sounds/3/3F#.wav")},
      {al_load_sample("sounds/4/4G.wav"), al_load_sample("sounds/4/4G#.wav"), al_load_sample("sounds/4/4A.wav"), al_load_sample("sounds/4/4A#.wav"), al_load_sample("sounds/4/4B.wav"), al_load_sample("sounds/4/4C.wav"), al_load_sample("sounds/4/4C#.wav")}};

   ALLEGRO_BITMAP *uke = al_load_bitmap("images/uke.png");
   if (!uke)
   {
      log("couldn't load uke\n");
      return 1;
   }

   al_register_event_source(queue, al_get_keyboard_event_source());
   al_register_event_source(queue, al_get_display_event_source(disp));
   al_register_event_source(queue, al_get_timer_event_source(timer));
   int ONE = 0;
   int TWO = 0;
   int THREE = 0;
   int FOUR = 0; 
   bool done = false;
   bool redraw = true;
   ALLEGRO_EVENT event;
   al_start_timer(timer);
   while (1)
   {
      al_wait_for_event(queue, &event);

      switch (event.type)
      {
      case ALLEGRO_EVENT_TIMER:
         redraw = true;
         break;

      case ALLEGRO_EVENT_KEY_DOWN:
   //They call me Yandere Dev 
         //! String 4
         
         if (event.keyboard.keycode == ALLEGRO_KEY_Z) FOUR = 1;
         if (event.keyboard.keycode == ALLEGRO_KEY_X) FOUR = 2;
         if (event.keyboard.keycode == ALLEGRO_KEY_C) FOUR = 3;
         if (event.keyboard.keycode == ALLEGRO_KEY_V) FOUR = 4;
         if (event.keyboard.keycode == ALLEGRO_KEY_B) FOUR = 5;
         if (event.keyboard.keycode == ALLEGRO_KEY_N) FOUR = 6;
         if (event.keyboard.keycode == ALLEGRO_KEY_M) FOUR = 7;
         
         //! String 3
         if (event.keyboard.keycode == ALLEGRO_KEY_A)THREE = 1;
         if (event.keyboard.keycode == ALLEGRO_KEY_S)THREE = 2;
         if (event.keyboard.keycode == ALLEGRO_KEY_D)THREE = 3;
         if (event.keyboard.keycode == ALLEGRO_KEY_F)THREE = 4;
         if (event.keyboard.keycode == ALLEGRO_KEY_G)THREE = 5;
         if (event.keyboard.keycode == ALLEGRO_KEY_H)THREE = 6;
         if (event.keyboard.keycode == ALLEGRO_KEY_J)THREE = 7;

         //! String 2
         if (event.keyboard.keycode == ALLEGRO_KEY_Q)TWO = 1;
         if (event.keyboard.keycode == ALLEGRO_KEY_W)TWO = 2;
         if (event.keyboard.keycode == ALLEGRO_KEY_E)TWO = 3;
         if (event.keyboard.keycode == ALLEGRO_KEY_R)TWO = 4;
         if (event.keyboard.keycode == ALLEGRO_KEY_T)TWO = 5;
         if (event.keyboard.keycode == ALLEGRO_KEY_Y)TWO = 6;
         if (event.keyboard.keycode == ALLEGRO_KEY_U)TWO = 7;
         
         //! String 1
         if (event.keyboard.keycode == ALLEGRO_KEY_1)ONE = 1;
         if (event.keyboard.keycode == ALLEGRO_KEY_2)ONE = 2;
         if (event.keyboard.keycode == ALLEGRO_KEY_3)ONE = 3;
         if (event.keyboard.keycode == ALLEGRO_KEY_4)ONE = 4;
         if (event.keyboard.keycode == ALLEGRO_KEY_5)ONE = 5;
         if (event.keyboard.keycode == ALLEGRO_KEY_6)ONE = 6;
         if (event.keyboard.keycode == ALLEGRO_KEY_7)ONE = 7;
         
         if(event.keyboard.keycode == ALLEGRO_KEY_LCTRL){
            al_play_sample(fretboardSamples[0][THREE], 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            al_rest(.2);
            al_play_sample(fretboardSamples[1][FOUR], 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
}
         
         
         if (event.keyboard.keycode != ALLEGRO_KEY_ESCAPE)
            break;
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
         done = true;
         break;
      }

      if (done)
         break;

      if (redraw && al_is_event_queue_empty(queue))
      {
         al_clear_to_color(al_map_rgb(99, 155, 255));
         al_draw_scaled_bitmap(uke, 0, 0, 900, 750, 10, 100, 1200, 1000, 1);

         // Draw Strings
         al_draw_ellipse(650, 283, 355, .1, al_map_rgb(69, 40, 60), 3);
         al_draw_ellipse(650, 303, 355, .1, al_map_rgb(69, 40, 60), 3);
         al_draw_ellipse(650, 323, 355, .1, al_map_rgb(69, 40, 60), 3);
         al_draw_ellipse(650, 343, 355, .1, al_map_rgb(69, 40, 60), 3);

         al_flip_display();

         redraw = false;
      }
   }
   al_destroy_bitmap(uke);
   for(int i = 0; i < 7; i++){
      al_destroy_sample(fretboardSamples[0][i]);
   }

   return 0;
}
